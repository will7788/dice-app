﻿namespace Dice_App
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.dieBox1 = new System.Windows.Forms.TextBox();
            this.dieBox2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Font = new System.Drawing.Font("Onyx", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 100);
            this.button1.TabIndex = 0;
            this.button1.Text = "Roll Dice";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // dieBox1
            // 
            this.dieBox1.BackColor = System.Drawing.Color.White;
            this.dieBox1.Font = new System.Drawing.Font("Onyx", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dieBox1.Location = new System.Drawing.Point(118, 22);
            this.dieBox1.Name = "dieBox1";
            this.dieBox1.ReadOnly = true;
            this.dieBox1.Size = new System.Drawing.Size(84, 81);
            this.dieBox1.TabIndex = 1;
            this.dieBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dieBox2
            // 
            this.dieBox2.BackColor = System.Drawing.Color.White;
            this.dieBox2.Font = new System.Drawing.Font("Onyx", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dieBox2.Location = new System.Drawing.Point(208, 22);
            this.dieBox2.Name = "dieBox2";
            this.dieBox2.ReadOnly = true;
            this.dieBox2.Size = new System.Drawing.Size(84, 81);
            this.dieBox2.TabIndex = 2;
            this.dieBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(301, 124);
            this.Controls.Add(this.dieBox2);
            this.Controls.Add(this.dieBox1);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Snake Eyes";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox dieBox1;
        private System.Windows.Forms.TextBox dieBox2;
    }
}

