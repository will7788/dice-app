﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dice_App
{
    public partial class Form1 : Form
    {
        private Dice die1 = new Dice();
        private Dice die2 = new Dice();
        int counter = 0;
        int die1val = 0;
        int die2val = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            die1val = die1.rollDie();
            int t = die1.rollDie();
            die2val = die2.rollDie();
            counter++;
            dieBox1.Text = die1val + "";
            dieBox2.Text = die2val + "";
            if (die1val == 1 && die2val == 1)
            {
                MessageBox.Show("It took " + counter + " rolls to get snake eyes!");
                counter = 0;
            }
            
        }
    }
}
