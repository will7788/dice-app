﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dice_App
{
    class Dice
    {
        private int sides;
        private int sideUp;
        Random rand = new Random();

        public Dice()
        {
            sides = 6;
        }

        public int rollDie()
        {
            sideUp = rand.Next(1,sides+1);
            return sideUp;
        }
    }
}
